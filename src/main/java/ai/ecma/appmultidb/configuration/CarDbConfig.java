package ai.ecma.appmultidb.configuration;

import ai.ecma.appmultidb.entity.car.Car;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * BY SIROJIDDIN on 23.11.2020
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "ai.ecma.appmultidb.repository.car",
        entityManagerFactoryRef = "carEntityManagerFactory",
        transactionManagerRef = "carTransactionManager")
public class CarDbConfig {


    @Value("${app.datasource.car.ddl.mode}")
    private String ddlMode;

    @Bean(name = "carDbProperties")
    @Primary
    @ConfigurationProperties("app.datasource.car")
    public DataSourceProperties carDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource carDataSource(@Qualifier("carDbProperties") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Primary
    @Bean(name = "carEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean carEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        return builder
                .dataSource(carDataSource(carDataSourceProperties()))
                .packages("ai.ecma.appmultidb.entity.car")
                .properties(properties)
                .build();
    }

    @Primary
    @Bean(name = "carTransactionManager")
    public PlatformTransactionManager carTransactionManager(
            final @Qualifier("carEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
