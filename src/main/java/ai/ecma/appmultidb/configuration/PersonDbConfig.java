package ai.ecma.appmultidb.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * BY SIROJIDDIN on 23.11.2020
 */

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "ai.ecma.appmultidb.repository.person",
        entityManagerFactoryRef = "personEntityManagerFactory",
        transactionManagerRef = "personTransactionManager")
public class PersonDbConfig {


    @Value("${app.datasource.person.ddl.mode}")
    private String ddlMode;

    @Bean(name = "personDbProperties")
    @ConfigurationProperties("app.datasource.person")
    public DataSourceProperties personDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource personDataSource(@Qualifier("personDbProperties") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean(name = "personEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean personEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.hbm2ddl.auto", ddlMode);
        return builder
                .dataSource(personDataSource(personDataSourceProperties()))
                .packages("ai.ecma.appmultidb.entity.person")
                .properties(properties)
                .build();
    }

    @Bean(name = "personTransactionManager")
    public PlatformTransactionManager personTransactionManager(
            final @Qualifier("personEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
