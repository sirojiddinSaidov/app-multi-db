package ai.ecma.appmultidb.entity.car;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * BY SIROJIDDIN on 30.11.2020
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "car_year")
public class CarYear {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer value;
}
