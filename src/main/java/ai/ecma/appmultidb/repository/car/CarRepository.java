package ai.ecma.appmultidb.repository.car;

import ai.ecma.appmultidb.entity.car.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "car")
public interface CarRepository extends JpaRepository<Car, Integer> {
}
