package ai.ecma.appmultidb.repository.car;

import ai.ecma.appmultidb.entity.car.Car;
import ai.ecma.appmultidb.entity.car.CarYear;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarYearRepository extends JpaRepository<CarYear, Integer> {
}
