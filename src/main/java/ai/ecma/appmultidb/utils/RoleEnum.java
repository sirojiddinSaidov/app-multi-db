package ai.ecma.appmultidb.utils;

public enum RoleEnum {
    ROLE_USER,
    ROLE_ADMIN
}
